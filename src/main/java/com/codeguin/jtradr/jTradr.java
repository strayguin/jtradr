package com.codeguin.jtradr;

import java.util.HashMap;

import com.codeguin.jtradr.ExchangeInterface;
import com.codeguin.jtradr.ConfigParser;

public class jTradr
{

	public static void main (String[] args)
	{
		String configFile = "mtgox.conf";
		String query = "";
		String oid = "";
		float price = 0;
		float amount = 0;

		HashMap authMap = null;

		ExchangeInterface httpI = null;

		if (0 == args.length)
		{
			print_help();
			return;
		}

		for (int i=0; i<args.length; i++)
		{
			switch (args[i])
			{
				case "-c":
					if (i+1 < args.length)
					{
						configFile = args[++i];
					}
					break;
				case "-h":
					print_help();
					return;
				default:
					query = args[i];

					if (query.equals("buy") || query.equals("sell"))
					{
						amount = Float.parseFloat(args[++i]);
						price  = Float.parseFloat(args[++i]);
					}
					else if (query.equals("cancel"))
					{
						oid = args[++i];
					}
			}
		}

		httpI = new MtGoxInterface();

		authMap = (new ConfigParser()).parse_file(configFile);
		httpI.authenticate(authMap);

		if (!query.equals(""))
		{
			switch (query)
			{
				case "account":
					System.out.println(httpI.get_account());
					break;
				case "buy":
					System.out.println(httpI.place_order("bid",price,amount));
					break;
				case "sell":
					System.out.println(httpI.place_order("ask",price,amount));
					break;
				case "cancel":
					System.out.println(httpI.cancel_order(oid));
					break;
				case "depth":
					System.out.println(httpI.get_depth());
					break;
				case "orders":
					System.out.println(httpI.get_orders());
					break;
				case "ticker":
					System.out.println(httpI.get_ticker());
					break;
				default:
					System.out.println("'"+query+"' is not a valid query.");
					break;
			}
		}
	}
	public static void print_help()
	{
		System.out.println("Usage:\n" +
		                   "    jTradr.java [-c <configfile>] <query> [additional parameters]\n" +
		                   "\n" +
		                   "Public Queries:\n" +
		                   "    ticker\n" +
		                   "    depth\n" +
		                   "\n" +
		                   "Private Queries:\n" +
		                   "    account\n" +
		                   "    buy    <amount> <price>\n" +
		                   "    sell   <amount> <price>\n" +
		                   "    cancel <order_id>\n" +
		                   "    orders\n");
	}
}
