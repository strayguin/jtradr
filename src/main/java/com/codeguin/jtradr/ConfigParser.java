package com.codeguin.jtradr;

import com.codeguin.jtradr.ConfigParser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ConfigParser
{
	public ConfigParser()
	{
	}

	public HashMap parse_file(String fname)
	{
		HashMap configs = new HashMap();

		String buffer;

		String key;
		String value;

		try
		{
			FileInputStream fileStream = new FileInputStream(fname);
			InputStreamReader inStream = new InputStreamReader(fileStream, "UTF-8");
			BufferedReader strStream = new BufferedReader(inStream);

			while ((buffer = strStream.readLine()) != null) {
				if (buffer.matches("^[\\s]*#")) {
					continue;
				}

				key = (buffer.split("=", 2))[0];
				value = (buffer.split("=", 2))[1];

				configs.put(key, value);
			}
		}
		catch(FileNotFoundException e)
		{
			configs.put("error", "file not found");
		}
		catch(UnsupportedEncodingException e)
		{
			configs.put("error", "unsupported encoding");
		}
		catch(IOException e)
		{
			configs.put("error", "error reading from file");
		}

		return configs;
	}
}
