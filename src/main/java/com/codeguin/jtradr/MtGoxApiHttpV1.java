package com.codeguin.jtradr;

import org.apache.commons.codec.binary.Base64;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;

public class MtGoxApiHttpV1
{
	private String pathRoot = "https://data.mtgox.com/api/1/";
	private boolean authentic = false;
	private String apiKey = "";
	private String apiSecret = "";

	public MtGoxApiHttpV1()
	{
	}

	public void authenticate(String key, String secret)
	{
		this.apiKey = key;
		this.apiSecret = secret;
		this.authentic = true;
	}

	public String get_account()
	{
		String path = "generic/private/info";
		return query(path, "", true);
	}

	public String add_order(String side, int price, int amount)
	{
		String path = "BTCUSD/private/order/add";
		String post;

		post = "type=" + side;
		post += "&price_int=" + Integer.toString(price);
		post += "&amount_int=" + Integer.toString(amount);

		return query(path, post, true);
	}

	public String cancel_order(String oid)
	{
		String path = "BTCUSD/private/order/cancel";
		String post = "oid=" + oid;

		return query(path, post, true);
	}

	public String get_orders()
	{
		String path = "generic/private/orders";
		return query(path, "", true);
	}

	public String get_ticker()
	{
		String path = "BTCUSD/ticker";
		return query(path, "", false);
	}

	public String get_depth()
	{
		String path = "BTCUSD/depth";
		return query(path, "", false);
	}

	private String query(String path, String poststr, boolean secure)
	{
		URL url;
		HttpURLConnection httpConn;
		String nonce;
		InputStream inStream;
		BufferedReader bReader;
		String response;

		if (true == secure && false == this.authentic)
		{
			return "{\"error\":\"not authorized\"}";
		}

		try
		{
			url = new URL(pathRoot + path);
			httpConn = (HttpURLConnection) url.openConnection();

			httpConn.setRequestProperty("User-Agent", "GoxBot");
			httpConn.setReadTimeout(5000);
			httpConn.setDoInput(true);

			if (true == secure)
			{
				if (0 < poststr.length())
				{
					poststr += "&";
				}

				poststr += "nonce=" + String.valueOf(System.currentTimeMillis() * 100);

				httpConn.setRequestProperty("Rest-Key", apiKey);
				httpConn.setRequestProperty("Rest-Sign", sign_post(poststr));
			}

			if (0 < poststr.length())
			{
				httpConn.setDoOutput(true);
				httpConn.getOutputStream().write(poststr.getBytes());
			}

			inStream = httpConn.getInputStream();
			bReader = new BufferedReader(new InputStreamReader(inStream));

			response = bReader.readLine();
		}
		catch(NoSuchAlgorithmException e)
		{
			response = "{\"error\":\"invalid algorithm?\"}";
		}
		catch(InvalidKeyException e)
		{
			response = "{\"error\":\"invalid key?\"}";
		}
		catch(MalformedURLException e)
		{
			response = "{\"error\":\"you done goofed\"}";
		}
		catch(IOException e)
		{
			response = "{\"error\":\"" + e.toString() + "\"}";
		}

		return response;
	}

	private String sign_post(String data) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException
	{
		Base64 b64;
		Mac mac;
		SecretKeySpec spec;
		String signed;

		b64 = new Base64();
		mac = Mac.getInstance("HmacSHA512");
		spec = new SecretKeySpec(b64.decodeBase64(this.apiSecret.getBytes()), "HmacSHA512");
		mac.init(spec);

		return new String(b64.encodeBase64(mac.doFinal(data.getBytes())), "UTF-8");
	}

}
