package com.codeguin.jtradr;

import java.util.HashMap;

import com.codeguin.jtradr.ExchangeInterface;

public class MtGoxInterface implements ExchangeInterface
{
	MtGoxApiHttpV1 hook;

	public MtGoxInterface()
	{
		hook = new MtGoxApiHttpV1();
	}

	public void authenticate(HashMap credentials)
	{
		hook.authenticate((String)credentials.get("key"), (String)credentials.get("secret"));
	}

	public String get_account()
	{
		return hook.get_account();
	}

	public String place_order(String side, float price, float amount)
	{
		int iprice = (int)(price * 100000);
		int iamount = (int)(amount * 100000000);

		return hook.add_order(side, iprice, iamount);
	}

	public String cancel_order(String oid)
	{
		return hook.cancel_order(oid);
	}

	public String get_depth()
	{
		return hook.get_depth();
	}

	public String get_orders()
	{
		return hook.get_orders();
	}

	public String get_ticker()
	{
		return hook.get_ticker();
	}
}
