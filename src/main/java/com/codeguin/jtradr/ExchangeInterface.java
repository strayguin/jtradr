package com.codeguin.jtradr;

import java.util.HashMap;

public interface ExchangeInterface
{
	public void authenticate(HashMap credentials);

	public String get_account();

	public String place_order(String side, float price, float amount);

	public String cancel_order(String oid);

	public String get_ticker();

	public String get_depth();

	public String get_orders();
}
